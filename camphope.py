from flask import Flask, render_template, request, flash
import smtplib
from email.mime.text import MIMEText

app = Flask(__name__)
app.secret_key = 'tTHFuwkBc1MK0wMcoxp5ulxsCBEjb6aQkSMuAy7f4slQf5zUXgbUAG9jf8QirvkLRYLKC2DdnthqD1lvYtxgZJiZQaunpshnG4gTpthjzZvi45kimFffpYKw'

@app.route("/", methods=['GET', 'POST'])
def main():
	if request.method == 'POST':
		message = request.form['message']
		name = request.form['name']
		email = request.form['email']
		phone = request.form['phone']
		msg = MIMEText(message+"\n\n\nName: %s\nEmail: %s\nPhone: %s" % (name, email, phone))
		msg['Subject'] = 'Message from camphopesc.com Form'
		msg['From'] = "CampHope@camphopesc.com"
		msg['To'] = "camphopespartanburg@gmail.com"
		#msg['To'] = "mayfieldiv@gmail.com"
		s = smtplib.SMTP('localhost')
		s.sendmail(msg['From'], msg['To'], msg.as_string())
		flash("Message successfully sent!")
	return render_template('index.html')

if __name__ == "__main__":
	app.debug = True
	app.run(host="0.0.0.0", port=8080)
